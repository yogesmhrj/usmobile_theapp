package com.vedamic.usmobiletheapp_alpha.views;

import android.app.Activity;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.utils.Helpers;

/**
 * Created by yogesh on 4/19/16.
 */
public class DialpadInterface {
    
    private BottomSheetLayout bottomSheetLayout;
    private Activity activity;
    private TextView dialpadNumberField;

    public DialpadInterface(Activity activity, BottomSheetLayout bottomSheetLayout, TextView dialpadNumberField) {
        this.bottomSheetLayout = bottomSheetLayout;
        this.activity = activity;
        this.dialpadNumberField = dialpadNumberField;
    }

    public void showDialPad(){

        View dialpad = activity.getLayoutInflater().inflate(R.layout.layout_dialpad_sheet,null,false);

        LinearLayout key1,key2,key3,key4,key5,key6,key7,key8,key9,key0,keyStar,keyHash;
        ImageButton dialpadAddContact,dialpadCall,dialpadBackSpace;

        this.dialpadNumberField = (TextView) dialpad.findViewById(R.id.dialpadNumber);

        key1 = (LinearLayout) dialpad.findViewById(R.id.dial_1);
        key2 = (LinearLayout) dialpad.findViewById(R.id.dial_2);
        key3 = (LinearLayout) dialpad.findViewById(R.id.dial_3);
        key4 = (LinearLayout) dialpad.findViewById(R.id.dial_4);
        key5 = (LinearLayout) dialpad.findViewById(R.id.dial_5);
        key6 = (LinearLayout) dialpad.findViewById(R.id.dial_6);
        key7 = (LinearLayout) dialpad.findViewById(R.id.dial_7);
        key8 = (LinearLayout) dialpad.findViewById(R.id.dial_8);
        key9 = (LinearLayout) dialpad.findViewById(R.id.dial_9);
        key0 = (LinearLayout) dialpad.findViewById(R.id.dial_0);
        keyStar = (LinearLayout) dialpad.findViewById(R.id.dialpadStar);
        keyHash = (LinearLayout) dialpad.findViewById(R.id.dialpadHash);

        dialpadAddContact = (ImageButton) dialpad.findViewById(R.id.dialpadAddToContacts);
        dialpadCall = (ImageButton) dialpad.findViewById(R.id.dialpadCall);
        dialpadBackSpace = (ImageButton) dialpad.findViewById(R.id.dialpadBackspace);

        key1.setOnClickListener(dialpadKeysClickListener);
        key2.setOnClickListener(dialpadKeysClickListener);
        key3.setOnClickListener(dialpadKeysClickListener);
        key4.setOnClickListener(dialpadKeysClickListener);
        key5.setOnClickListener(dialpadKeysClickListener);
        key6.setOnClickListener(dialpadKeysClickListener);
        key7.setOnClickListener(dialpadKeysClickListener);
        key8.setOnClickListener(dialpadKeysClickListener);
        key9.setOnClickListener(dialpadKeysClickListener);
        keyStar.setOnClickListener(dialpadKeysClickListener);
        keyHash.setOnClickListener(dialpadKeysClickListener);
        key0.setOnClickListener(dialpadKeysClickListener);
        key0.setOnLongClickListener(dialpadLongPressOn0);

        dialpadAddContact.setOnClickListener(dialpadActionButtonListener);
        dialpadCall.setOnClickListener(dialpadActionButtonListener);
        dialpadBackSpace.setOnClickListener(dialpadActionButtonListener);

        this.bottomSheetLayout.showWithSheetView(dialpad);
    }

    private View.OnClickListener dialpadKeysClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int volume = 50;
            ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_MUSIC,volume);
            switch (view.getId()){
                case R.id.dial_1:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_1, 120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString()+"1");
                    break;
                case R.id.dial_2:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_2, 120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString() + "2");
                    break;
                case R.id.dial_3:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_3, 120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString() + "3");
                    break;
                case R.id.dial_4:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_4, 120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString() + "4");
                    break;
                case R.id.dial_5:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_5, 120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString() + "5");
                    break;
                case R.id.dial_6:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_6, 120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString() + "6");
                    break;
                case R.id.dial_7:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_7, 120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString() + "7");
                    break;
                case R.id.dial_8:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_8, 120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString() + "8");
                    break;
                case R.id.dial_9:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_9, 120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString() + "9");
                    break;
                case R.id.dial_0:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_0, 120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString() + "0");
                    break;
                case R.id.dialpadStar:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_1, 120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString() + "*");
                    break;
                case R.id.dialpadHash:
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_DTMF_3,120);
                    dialpadNumberField.setText(dialpadNumberField.getText().toString() + "#");
                    break;
                default:
                    toneGenerator.stopTone();
                    break;
            }

        }
    };

    private View.OnLongClickListener dialpadLongPressOn0 = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            return false;
        }
    };

    private View.OnClickListener dialpadActionButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.dialpadAddToContacts:
                    //action to add to contacts
                    break;
                case R.id.dialpadCall:
                    //action to call
                    Helpers.makeCall(activity,dialpadNumberField.getText().toString());
                    //bottomSheetLayout.dismissSheet();
                    break;
                case R.id.dialpadBackspace:
                    //action for backspace
                    String number = dialpadNumberField.getText().toString();
                    if(number.length() > 1){
                        dialpadNumberField.setText(number.substring(0,number.length() - 1));
                    }
                    else{
                        dialpadNumberField.setText("");
                    }
                    break;
                default:
                    break;
            }
        }
    };

    public TextView getDialpadNumberField() {
        return dialpadNumberField;
    }

    public BottomSheetLayout getBottomSheetLayout() {
        return bottomSheetLayout;
    }
}
