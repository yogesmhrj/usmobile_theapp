package com.vedamic.usmobiletheapp_alpha.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.squareup.picasso.Picasso;
import com.vedamic.usmobiletheapp_alpha.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A login screen that offers login via the users phone number.
 *
 * The screen contains both the login screen and also the verification screen
 * managed by the states of the activity.
 */
public class LoginActivity extends AppCompatActivity{

    LinearLayout loginForm,verificationForm;
    EditText phoneNumber,otpCode;
    Button submitPhone,submitOtpCode;
    Spinner countryList;

    List<String> countries = new ArrayList<>();
    String selectedCountry = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //find the views necessary
        findViews();

        //load the background pattern for the screen
        Picasso.with(this)
                .load(R.drawable.splash_background_min)
                .into((ImageView) findViewById(R.id.splashBackground));

        //set the spinner
        populateCountryList();
        this.countryList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, countries));
    }

    private void findViews(){
        this.loginForm = (LinearLayout) findViewById(R.id.loginForm);
        this.verificationForm = (LinearLayout) findViewById(R.id.verificationForm);
        this.phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        this.otpCode = (EditText) findViewById(R.id.optCode);
        this.submitPhone = (Button) findViewById(R.id.submitPhoneNumber);
        this.submitOtpCode = (Button) findViewById(R.id.submitOTPCode);
        this.countryList = (Spinner) findViewById(R.id.countryList);
    }

    @Override
    protected void onStart() {
        super.onStart();

        this.countryList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                selectedCountry = countries.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });

        this.phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (phoneNumber.getText().toString().length() > 6) {
                    submitPhone.setEnabled(true);
                } else {
                    submitPhone.setEnabled(false);
                }
            }
        });

        this.otpCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otpCode.getText().toString().length() > 3) {
                    submitOtpCode.setEnabled(true);
                } else {
                    submitOtpCode.setEnabled(false);
                }
            }
        });

        this.submitPhone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loginForm.setVisibility(View.GONE);
                verificationForm.setVisibility(View.VISIBLE);
                //also need to hide the soft keyboard or focus on the code text fields
                otpCode.requestFocus();
            }
        });

        this.submitOtpCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, Dashboard.class));
                finish();
            }
        });
    }

    private boolean populateCountryList(){
        this.countries.add("Argentina(+54)");
        this.countries.add("Australia(+61)");
        this.countries.add("Bangladesh(+880)");
        this.countries.add("Brazil(+55)");
        this.countries.add("Canada(+1)");
        this.countries.add("China(+86)");
        this.countries.add("Denmark(+45)");
        this.countries.add("France(+33)");
        this.countries.add("Nepal(+977)");
        this.countries.add("USA(+1)");
        return true;
    }

}

