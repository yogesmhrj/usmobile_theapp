package com.vedamic.usmobiletheapp_alpha.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.vedamic.usmobiletheapp_alpha.R;

public class SplashScreen extends AppCompatActivity {

    Button continueButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        findViews();

        //load the background pattern for the screen
        Picasso.with(this)
                .load(R.drawable.splash_background_min)
                .into((ImageView) findViewById(R.id.splashBackground));

    }

    private void findViews(){
        this.continueButton = (Button) findViewById(R.id.continueButton);
    }

    @Override
    protected void onStart() {
        super.onStart();

        //set the listener for the continue button and then forward to the login activity
        this.continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashScreen.this,LoginActivity.class));
                finish();
            }
        });
    }
}
