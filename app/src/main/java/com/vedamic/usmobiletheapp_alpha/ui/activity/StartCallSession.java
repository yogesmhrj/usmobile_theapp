package com.vedamic.usmobiletheapp_alpha.ui.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.app.exceptions.ActivityMemberIncompleteException;
import com.vedamic.usmobiletheapp_alpha.app.objects.ContactDetail;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;
import com.vedamic.usmobiletheapp_alpha.utils.ImageRenderer;
import com.vedamic.usmobiletheapp_alpha.utils.Utils;

import java.util.Timer;
import java.util.TimerTask;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;

public class StartCallSession extends AppCompatActivity implements SensorEventListener{

    private static final String TAG = "==> StartCallSession";
    /*
     ------------------------------------------------------------------------------
     | TO start a call session, ContactDetail item must be passed.
     ------------------------------------------------------------------------------
    */

    //the views
    private TextView callingState,calledUsername,calledUserNum,calledUserduration,networkUnstable;
    private ImageView calledUserImage;
    private ImageButton endCall;

    private ContactDetail selectedContact;
    private Timer callTimer;
    private int callDuration = 0;
    private Bitmap userPictureBitmap;
    private SetUserProfilePicture setUserProfilePicture;
    private SensorManager sensorManager;
    private Sensor proximity;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_call_session);

        try {
            this.selectedContact = (ContactDetail) getIntent().getSerializableExtra(Constants.SELECTED_CONTACT);

            if (this.selectedContact == null) {
                throw new ActivityMemberIncompleteException("ContactDetail must be passed for this activity to work");
            }
        }catch (Exception e){
            Log.e(TAG, "onCreate: "+e.getMessage());
            finish();
        }

        getWindow().addFlags(FLAG_KEEP_SCREEN_ON);
        //or use
        //Utils.keepScreenOn(this);

        findViews();

        this.sensorManager  = (SensorManager) getSystemService(SENSOR_SERVICE);
        this.proximity = this.sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        setProximitySensors();

        this.setUserProfilePicture =new SetUserProfilePicture();
        this.setUserProfilePicture.execute();

        //set the user details
//        Picasso.with(this)
//                .load(this.selectedContact.getProfileImageUri())
//                .into(this.calledUserImage);

        //blur the image
//        Blurry.with(this)
//                .radius(10)
//                .sampling(1)
//                .async()
//                .capture(this.calledUserImage)
//                .into(this.calledUserImage);

        //mimic a calling state
        processCall();

    }

    private void findViews(){

        this.callingState = (TextView) findViewById(R.id.callRingStatus);
        this.callingState.setVisibility(View.GONE);
        this.networkUnstable = (TextView) findViewById(R.id.callNetworkQuality);
        this.calledUserImage = (ImageView) findViewById(R.id.callUserProfilePic);
        this.calledUsername = (TextView) findViewById(R.id.callUserName);
        this.calledUserNum = (TextView) findViewById(R.id.callUserNumber);
        this.calledUserduration = (TextView) findViewById(R.id.callUserDuration);

        this.endCall = (ImageButton) findViewById(R.id.actionCallEnd);

        this.calledUsername.setText(this.selectedContact.getName());
        this.calledUserNum.setText(this.selectedContact.getPhone());
    }

    @Override
    protected void onStart() {
        super.onStart();

        this.endCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //end the call
                setCallingState(Constants.CallingStates.ENDING);
                processEndCall();
            }
        });
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float distance = sensorEvent.values[0];
        // Do something with this sensor data.

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private class SetUserProfilePicture extends AsyncTask<Void,Void,Boolean>{

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                userPictureBitmap = ImageRenderer.fastblur(ImageRenderer.getBitmapFromAsset(
                        StartCallSession.this, selectedContact.getProfileImageUri()), 13);
                return true;
            }catch (Exception e){
                return false;
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            onPostExecute(false);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean){
                calledUserImage.setImageBitmap(userPictureBitmap);
            }
        }
    }

    private void setProximitySensors(){
        if(this.proximity != null){
            this.sensorManager.registerListener(this,this.proximity,SensorManager.SENSOR_DELAY_NORMAL);
        }
        else{
            Log.d(TAG, "setProximitySensors: Device doesn't have proximity sensor.");
        }
    }

    private void setCallingState(Constants.CallingStates state){
        switch (state){
            case CALLING:
                this.callingState.setVisibility(View.VISIBLE);
                this.callingState.setText("Calling . . .");
                break;
            case RINGING:
                this.callingState.setVisibility(View.VISIBLE);
                this.callingState.setText("Ringing . . .");
                break;
            case TALKING:
                this.callingState.setVisibility(View.GONE);
                this.calledUserduration.setVisibility(View.VISIBLE);
                this.calledUserduration.setText("");
                break;
            case ENDING:
                this.callingState.setVisibility(View.VISIBLE);
                this.callingState.setText("Ending . . .");
                break;
        }
    }

    private void processCall(){


        /*
         ------------------------------------------------------------------------------
         | mimic process to connect the call to the network
         ------------------------------------------------------------------------------
        */
        setCallingState(Constants.CallingStates.CALLING);

        /*
         ------------------------------------------------------------------------------
         | mimic ringing state
         ------------------------------------------------------------------------------
        */
        setCallingState(Constants.CallingStates.RINGING);


        /*
         ------------------------------------------------------------------------------
         | mimic talking state
         ------------------------------------------------------------------------------
        */
        exchangeCalls();

    }

    private void exchangeCalls(){
        setCallingState(Constants.CallingStates.TALKING);

        callTimer = new Timer();
        callTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int minutes = callDuration / 60;
                        int second = callDuration % 60;
                        calledUserduration.setText(minutes + ":" + second);

                        if(callDuration % 60 == 0){
                            networkUnstable.setVisibility(View.VISIBLE);
                        }
                        if(callDuration % 60 == 10){
                            networkUnstable.setVisibility(View.GONE);
                        }

                        callDuration++;
                    }
                });
            }
        },1000,1000);

    }

    private void processEndCall(){
        if(this.callTimer != null){
            this.callTimer.cancel();
        }
        //mimic ending process
        //finally end the call and exit the activity
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setProximitySensors();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.sensorManager.unregisterListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(this.setUserProfilePicture != null){
            this.setUserProfilePicture.cancel(true);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
