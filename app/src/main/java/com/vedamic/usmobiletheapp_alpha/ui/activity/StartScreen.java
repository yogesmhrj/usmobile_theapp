package com.vedamic.usmobiletheapp_alpha.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class StartScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //check conditions and redirect to the activities
        if(true){
            //startActivity(new Intent(this,MainActivity.class));
            startActivity(new Intent(this,SplashScreen.class));
        }
        else{
            startActivity(new Intent(this,SplashScreen.class));
        }
        finish();
    }
}
