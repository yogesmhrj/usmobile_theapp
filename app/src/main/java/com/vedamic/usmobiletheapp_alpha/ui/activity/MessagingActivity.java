package com.vedamic.usmobiletheapp_alpha.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.app.objects.ChatListItem;
import com.vedamic.usmobiletheapp_alpha.ui.fragments.MessageNavigationFragment;
import com.vedamic.usmobiletheapp_alpha.ui.fragments.MessagingFragment;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;
import com.vedamic.usmobiletheapp_alpha.utils.Utils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MessagingActivity extends AppCompatActivity implements MessageNavigationFragment.ChatNavigationClickListener{

    private ListView navigationListview;
    private DrawerLayout drawerLayout;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        findViews();

        //get the selected chat passed through the intent
        ChatListItem selectedChat = (ChatListItem) getIntent().getSerializableExtra(Constants.SELECTED_CHAT_SESSION);

        //set the toolbar title
        getSupportActionBar().setTitle(selectedChat.getContactDetail().getName());

        FragmentManager fragmentManager = getSupportFragmentManager();

        //set up the navigation chatlist
        MessageNavigationFragment messageNavigationFragment = MessageNavigationFragment.newInstance(selectedChat);
        messageNavigationFragment.setChatNavigationClickListener(this);
        fragmentManager.beginTransaction().replace(R.id.navigationChatList,messageNavigationFragment).commit();

        //pass the selected chat to the fragment
        fragmentManager.beginTransaction().replace(R.id.messagingContainer,MessagingFragment.newInstance(selectedChat)).commit();

    }

    private void findViews(){
        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.navigationChatList);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, Utils.getStatusBarHeightRes(this), 0, 0);
        frameLayout.setLayoutParams(lp);


    }

    @Override
    protected void onStart() {
        super.onStart();
        this.drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.messaging, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        boolean isDrawerOpened = this.drawerLayout.isDrawerOpen(GravityCompat.START) || this.drawerLayout.isDrawerOpen(Gravity.LEFT);
        for(int i = 0; i < menu.size(); i++){
            menu.getItem(i).setVisible(!isDrawerOpened);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.action_more:
                break;
            case R.id.action_call:
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onChatNavigationItemClicked(int selectedPosition, ChatListItem selectedItem) {
        //need to handle the chatlist interactions here
        getSupportActionBar().setTitle(selectedItem.getContactDetail().getName());
        //pass the selected chat to the fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.messagingContainer,MessagingFragment.newInstance(selectedItem)).commit();
    }

    @Override
    public void closeDrawer() {
        if(this.drawerLayout.isDrawerOpen(GravityCompat.START)){
            this.drawerLayout.closeDrawer(GravityCompat.START);
        }
    }
}
