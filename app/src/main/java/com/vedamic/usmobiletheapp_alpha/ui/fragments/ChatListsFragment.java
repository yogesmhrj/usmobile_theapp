package com.vedamic.usmobiletheapp_alpha.ui.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.app.core.BaseApplication;
import com.vedamic.usmobiletheapp_alpha.app.objects.ChatListItem;
import com.vedamic.usmobiletheapp_alpha.ui.activity.MessagingActivity;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ChatListsFragment extends Fragment {

    //the layouts
    private LinearLayout emptylayout,successlayout;
    private ListView chatListView;

    protected List<ChatListItem> chatListItems = new ArrayList<>();

    private FetchChatList fetchChatList;

    public ChatListsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_chat_lists, container, false);
        findViews(fragmentView);

        return fragmentView;
    }

    protected void findViews(View fragmentView){
        this.emptylayout = (LinearLayout) fragmentView.findViewById(R.id.noChatSessionLayout);
        this.successlayout = (LinearLayout) fragmentView.findViewById(R.id.chatListLayout);
        this.chatListView = (ListView) fragmentView.findViewById(R.id.chatListView);

        setViewState(Constants.ViewState.LOADING);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.chatListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int selectedPosition, long l) {
                onChatListItemClicked(adapterView,view,selectedPosition,l);
            }
        });

        this.fetchChatList = new FetchChatList();
        this.fetchChatList.execute();
    }

    /**
     * Called after user clicks on a chat list item
     *
     * @param adapterView
     * @param view
     * @param selectedPosition
     * @param l
     */
    protected void onChatListItemClicked(AdapterView<?> adapterView, View view, int selectedPosition, long l){
        Intent messageActivity = new Intent(getActivity(), MessagingActivity.class);
        messageActivity.putExtra(Constants.SELECTED_CHAT_SESSION,chatListItems.get(selectedPosition));
        startActivity(messageActivity);
    }

    private void setViewState(Constants.ViewState viewState){
        switch (viewState){
            case SUCCESS:
                this.emptylayout.setVisibility(View.GONE);
                this.successlayout.setVisibility(View.VISIBLE);
                break;
            case FAILED:
            case EMPTY:
                this.emptylayout.setVisibility(View.VISIBLE);
                this.successlayout.setVisibility(View.GONE);
                break;
            default:
            case LOADING:
                this.emptylayout.setVisibility(View.GONE);
                this.successlayout.setVisibility(View.GONE);
                break;
        }
    }

    /**
     * Get the adapter to set to the chat list
     * @return
     */
    protected BaseAdapter getChatListAdapter(){
        return new ChatListViewAdapter();
    }

    private class ChatListViewAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return chatListItems.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = getActivity().getLayoutInflater().inflate(R.layout.item_chat_list_view,parent,false);
            }

            ChatListItem item = chatListItems.get(position);

            ImageView profilePic = (ImageView) convertView.findViewById(R.id.chatListItemIcon);
            TextView name = (TextView) convertView.findViewById(R.id.chatListItemName);
            TextView message = (TextView) convertView.findViewById(R.id.chatListItemMessage);
            TextView date   = (TextView) convertView.findViewById(R.id.chatListItemDate);

            Picasso.with(getActivity()).load(item.getContactDetail().getProfileThumbImageUri())
                    .placeholder(R.drawable.ic_icon_account_placeholder)
                    .resize(100,100).centerCrop().into(profilePic);

            name.setText(item.getContactDetail().getName());
            message.setText(item.getMessage());
            date.setText(item.getDateTime());

            return convertView;
        }

    }

    private class FetchChatList extends AsyncTask<Void,Void,Boolean>{

        @Override
        protected Boolean doInBackground(Void... voids) {
            //need to fetch the chat list history

            /*
             ------------------------------------------------------------------------------
             | Simply populate the data for now
             ------------------------------------------------------------------------------
            */
            chatListItems = BaseApplication.getDummyChatList();


            return !chatListItems.isEmpty();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            onPostExecute(false);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if(aBoolean){
                setViewState(Constants.ViewState.SUCCESS);
                chatListView.setAdapter(getChatListAdapter());
            }
            else{
                setViewState(Constants.ViewState.EMPTY);
            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(this.fetchChatList != null){
            this.fetchChatList.cancel(true);
        }
    }
}
