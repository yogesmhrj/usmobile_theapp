package com.vedamic.usmobiletheapp_alpha.ui.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.app.exceptions.ActivityMemberIncompleteException;
import com.vedamic.usmobiletheapp_alpha.app.objects.CallListItem;
import com.vedamic.usmobiletheapp_alpha.app.objects.ContactDetail;
import com.vedamic.usmobiletheapp_alpha.tasks.QueryCallSession;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ContactsPreview extends AppCompatActivity {
    private static final String TAG = "==> ContactsPreview";
    /*
     ------------------------------------------------------------------------------
     | This activity serves to provide information of the selected contact.
     | Thus it requires that a ContactDetail item be passed to it through the intent.
     ------------------------------------------------------------------------------
    */
    private ContactDetail selectedContact;
    private TextView phoneNumber;
    private ImageView profilePicture;
    private LinearLayout call,message,video;
    private ListView listView;
    private List<CallListItem> callListItemsList = new ArrayList<>();
    private QueryCallSession queryCallSession;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_preview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            //must have the ContactDetails
            this.selectedContact = (ContactDetail) getIntent().getSerializableExtra(Constants.SELECTED_CONTACT);
            if (this.selectedContact == null) {
                throw new ActivityMemberIncompleteException("ContactDetail not passed to the intent.");
            }
        }catch (Exception e){
            Log.e(TAG, "onCreate: "+e.getMessage());
            finish();
        }

        //set the toolbars
        getSupportActionBar().setTitle(this.selectedContact.getName());
        getSupportActionBar().setSubtitle(this.selectedContact.getPhone());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        findViews();
    }

    //find views
    private void findViews(){
        this.profilePicture = (ImageView) findViewById(R.id.contactProfilePicture);
        this.phoneNumber = (TextView) findViewById(R.id.contactsPhoneNumber);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //load the profile picture
        Picasso.with(this)
                .load(this.selectedContact.getProfileImageUri())
                .placeholder(R.drawable.ic_icon_account_placeholder)
                .into(this.profilePicture);
        //load the phone number
        this.phoneNumber.setText(this.selectedContact.getPhone());

        //query the call session for the contact
        this.queryCallSession = getQueryCallSession();
        this.queryCallSession.execute(this.selectedContact);

    }

    /**
     * Return a instance for the QueryCallSession task
     *
     * @return
     */
    private QueryCallSession getQueryCallSession(){
        //return a new instance for the QueryCallSession task
        return new QueryCallSession(this) {
            @Override
            public void onPostExecuteCall(Boolean aBoolean, List<CallListItem> callListItems) {
                callListItemsList = callListItems;
                if(aBoolean){

                }
            }
        };
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //simple switch case for the menu options
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
