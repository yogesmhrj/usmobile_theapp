package com.vedamic.usmobiletheapp_alpha.ui.fragments;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.app.objects.ContactDetail;
import com.vedamic.usmobiletheapp_alpha.ui.activity.ContactsPreview;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class ContactsListFragment extends Fragment{

    private List<ContactDetail> contactsList = new ArrayList<>();
    private ListView contactListView;

    public ContactsListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_contacts_list, container, false);
        findViews(view);
        //get the contact lists
        getNumber(getActivity().getContentResolver());

        return view;
    }

    private void findViews(View view){
        this.contactListView = (ListView) view.findViewById(R.id.contactsAllListView);
        this.contactListView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.contactListView.setAdapter(new ContactsAdapter());

        this.contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), ContactsPreview.class);
                //forward the selected contact item into the intent
                intent.putExtra(Constants.SELECTED_CONTACT,contactsList.get(i));
                startActivity(intent);
            }
        });
    }
    private static final String SORT_ORDER = ContactsContract.Data.MIMETYPE;

    public void getNumber(ContentResolver cr)
    {
//        Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                null,
//                ContactsContract.CommonDataKinds.Phone.CONTACT_ID+ " = ?",
//                new String[] { id },
//                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");

        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");
        ContactDetail contact = null;
        // use the cursor to access the contacts
        while (phones.moveToNext())
        {
            int contactId = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
            // get display name
            String name= phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            // get phone number
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            //get photo thumbnail
            String photoThumbUri = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
            //get photo uri
            String photoUri = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
            contact = new ContactDetail(contactId,name,phoneNumber,photoThumbUri);
            contact.setProfileImageUri(photoUri);

            this.contactsList.add(contact);

        }

    }

    private class ContactsAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return contactsList.size();
        }

        @Override
        public Object getItem(int i) {
            return contactsList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if(view == null){
                view = getActivity().getLayoutInflater().inflate(R.layout.item_contacts_list_view,viewGroup,false);
            }

            ContactDetail contact = (ContactDetail) getItem(i);

            ImageView contactImage = (ImageView) view.findViewById(R.id.contactPhoto);
            TextView contactName = (TextView) view.findViewById(R.id.contactName);
            Picasso.with(getActivity()).load(contact.getProfileThumbImageUri())
                    .placeholder(R.drawable.ic_icon_account_placeholder)
                    .resize(50,50).centerCrop()
                    .into(contactImage);
            contactName.setText(contact.getName());

            return view;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            return 1;
        }
    }

}
