package com.vedamic.usmobiletheapp_alpha.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.ui.fragments.CallListFragment;
import com.vedamic.usmobiletheapp_alpha.ui.fragments.ChatListsFragment;
import com.vedamic.usmobiletheapp_alpha.ui.fragments.ConferenceListFragment;
import com.vedamic.usmobiletheapp_alpha.ui.fragments.ContactsListFragment;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;
import com.vedamic.usmobiletheapp_alpha.utils.ImageRenderer;
import com.vedamic.usmobiletheapp_alpha.views.DialpadInterface;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = "==> Dashboard";

    private NavigationView navigationView;
    private BottomSheetLayout bottomSheetLayout;
    private MaterialTabHost tabHost;
    private ViewPager pager;
    private ViewPagerAdapter pagerAdapter;
    private FloatingActionButton addChats,addContact,dialpad;

    private DialpadInterface dialpadInterface;
    private TextView dialpadNumber;

    int[] tabIcons = new int[] {
            R.drawable.ic_main_chat,
            R.drawable.ic_main_contacts,
            R.drawable.ic_main_call,
            R.drawable.ic_main_conference
    };

    //default is the first page
    private Constants.DashboardPageState dashboardPageState = Constants.DashboardPageState.CHAT;

    @Override
    protected void attachBaseContext(Context newBase) {
        //for the custom font
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        this.navigationView = (NavigationView) findViewById(R.id.nav_view);
        this.navigationView.setNavigationItemSelectedListener(this);

        findViews();

        // init view pager
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.setOffscreenPageLimit(5);
        pager.setOnPageChangeListener(pagerPageChangeListener);

        // insert all tabs from pagerAdapter data
        for (int i = 0; i < pagerAdapter.getCount(); i++) {
            tabHost.addTab(
                    tabHost.newTab()
                            .setIcon(ContextCompat.getDrawable(this,tabIcons[i]))
                            //.setText(pagerAdapter.getPageTitle(i))
                            .setTabListener(materialTabListener)
            );

        }

        pager.setCurrentItem(0);
        tabHost.setSelectedNavigationItem(0);

    }

    private void findViews(){
        this.bottomSheetLayout = (BottomSheetLayout) this.findViewById(R.id.bottomSheetLayout);
        tabHost = (MaterialTabHost) this.findViewById(R.id.tabHost);
        pager = (ViewPager) this.findViewById(R.id.pager );
        this.addChats = (FloatingActionButton) findViewById(R.id.fabAddChats);
        this.addContact = (FloatingActionButton) findViewById(R.id.fabAddContacts);
        this.dialpad = (FloatingActionButton) findViewById(R.id.fabDialpad);

        //set the navigation drawer views
        View headerView = this.navigationView.getHeaderView(0);
        ImageView background = (ImageView)headerView.findViewById(R.id.navBackground);
        ImageView userProfilePic = (ImageView) headerView.findViewById(R.id.userProfilePic);
        TextView userName = (TextView) headerView.findViewById(R.id.username);

        background.setImageBitmap(ImageRenderer.fastblur(
                ImageRenderer.getBitmapFromAsset(this,"images/nav_back.jpeg"),12));

        userProfilePic.setImageBitmap(ImageRenderer.getRoundedCroppedBitmap(
                ImageRenderer.getBitmapFromAsset(this,"images/default_profile_pic.png")));

        //userName.setText("");
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.addChats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Dashboard.this, "Open option for adding chats", Toast.LENGTH_SHORT).show();
            }
        });

        this.addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Dashboard.this, "Open option for adding contacts", Toast.LENGTH_SHORT).show();
            }
        });

        this.dialpad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialpad();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            //check if any sheets are opened
            if (this.bottomSheetLayout.isSheetShowing()) {
                this.bottomSheetLayout.dismissSheet();
            }
            if (this.dialpadInterface != null && this.dialpadInterface.getBottomSheetLayout().isSheetShowing()) {
                this.dialpadInterface.getBottomSheetLayout().dismissSheet();
            }
        }catch (Exception e){
            Log.e(TAG, "onOptionsItemSelected: --> "+e.getMessage());
        }

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            //will open up the search
            Intent searchIntent = new Intent(this,SearchActivity.class);
            searchIntent.putExtra(Constants.DASHBOARD_PAGE_STATE,this.dashboardPageState);
            startActivity(searchIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        public Fragment getItem(int num) {
           switch (num){
               case 0:
                   return new ChatListsFragment();
               case 1:
                   return new ContactsListFragment();
               case 2:
                   return new CallListFragment();
               case 3:
                   return new ConferenceListFragment();
               default:
                   return new ChatListsFragment();
           }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch(position){
                case 0:
                    return "Chats";
                case 1:
                    return "Contacts";
                case 2:
                    return "Calls";
                case 3:
                    return "Conferences";
                default:
                   return "";
            }
        }

    }

    private MaterialTabListener materialTabListener = new MaterialTabListener() {
        @Override
        public void onTabSelected(MaterialTab tab) {
            pager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabReselected(MaterialTab tab) {

        }

        @Override
        public void onTabUnselected(MaterialTab tab) {

        }
    };

    private ViewPager.SimpleOnPageChangeListener pagerPageChangeListener = new ViewPager.SimpleOnPageChangeListener(){
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            // when user do a swipe the selected tab change
            tabHost.setSelectedNavigationItem(position);
            //set the pager state based on the changed pager page
            switch (position){
                case 0:
                    dashboardPageState = Constants.DashboardPageState.CHAT;
                    //then control the floating button states
                    addChats.setVisibility(View.VISIBLE);
                    addContact.setVisibility(View.GONE);
                    dialpad.setVisibility(View.GONE);
                    break;
                case 1:
                    dashboardPageState = Constants.DashboardPageState.CONTACT;
                    addChats.setVisibility(View.GONE);
                    addContact.setVisibility(View.VISIBLE);
                    dialpad.setVisibility(View.GONE);
                    break;
                case 2:
                    dashboardPageState = Constants.DashboardPageState.CALL;
                    addChats.setVisibility(View.GONE);
                    addContact.setVisibility(View.GONE);
                    dialpad.setVisibility(View.VISIBLE);
                    break;
                case 3:
                    dashboardPageState = Constants.DashboardPageState.CONFERENCE;
                    addChats.setVisibility(View.GONE);
                    addContact.setVisibility(View.GONE);
                    dialpad.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }
    };

    private void openDialpad(){
        //will display the dialpad
        this.dialpadInterface = new DialpadInterface(this,this.bottomSheetLayout,this.dialpadNumber);
        this.dialpadInterface.showDialPad();
    }


}
