package com.vedamic.usmobiletheapp_alpha.ui.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.app.objects.ChatListItem;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;

/**
 * Created by yogesh on 4/22/16.
 */
public class MessageNavigationFragment extends ChatListsFragment {
    private static final String TAG = "==> MessageNavigation";
    private ChatListItem currentlyselected;
    private ChatNavigationClickListener chatNavigationClickListener;

    public static MessageNavigationFragment newInstance(ChatListItem selectedItem){
        MessageNavigationFragment fragment = new MessageNavigationFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.SELECTED_CHAT_SESSION, selectedItem);
        fragment.setArguments(bundle);
        return fragment;
    }

    public MessageNavigationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            this.currentlyselected = (ChatListItem) getArguments().getSerializable(Constants.SELECTED_CHAT_SESSION);
        }
    }

    @Override
    protected BaseAdapter getChatListAdapter() {
        return new ChatListAdapter();
    }

    private class ChatListAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return chatListItems.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = getActivity().getLayoutInflater().inflate(R.layout.item_nav_chat_list_view,parent,false);
            }

            ChatListItem item = chatListItems.get(position);

            ImageView profilePic = (ImageView) convertView.findViewById(R.id.chatListItemIcon);
            TextView name = (TextView) convertView.findViewById(R.id.chatListItemName);
            TextView message = (TextView) convertView.findViewById(R.id.chatListItemMessage);
            TextView date   = (TextView) convertView.findViewById(R.id.chatListItemDate);

            Picasso.with(getActivity()).load(item.getContactDetail().getProfileThumbImageUri())
                    .placeholder(R.drawable.ic_icon_account_placeholder)
                    .resize(100,100).centerCrop().into(profilePic);

            name.setText(item.getContactDetail().getName());
            message.setText(item.getMessage());
            date.setText(item.getDateTime());

            return convertView;
        }

    }

    public void setChatNavigationClickListener(ChatNavigationClickListener chatNavigationClickListener) {
        this.chatNavigationClickListener = chatNavigationClickListener;
    }

    /**
     * Handle item click of the chat lists in the navigation drawer
     * <br><br>
     *     No need to handle the closing of the drawer. The method calls to closeDrawer()
     *     of the interface after its execution.
     *
     * @param adapterView
     * @param view
     * @param selectedPosition
     * @param l
     */
    @Override
    protected void onChatListItemClicked(AdapterView<?> adapterView, View view, int selectedPosition, long l) {
        if(this.chatNavigationClickListener == null){
            Log.e(TAG, "onChatListItemClicked: Listener need to be initialized by calling setChatNavigationClickListener().");
        }
        else{
            //call the message activity to replace the fragment
            if(this.chatListItems.get(selectedPosition).getChatId() != currentlyselected.getChatId()){
                this.currentlyselected = this.chatListItems.get(selectedPosition);
                this.chatNavigationClickListener.onChatNavigationItemClicked(selectedPosition,this.chatListItems.get(selectedPosition));
                this.chatNavigationClickListener.closeDrawer();
            }
            else{
                this.chatNavigationClickListener.closeDrawer();
            }
        }
    }

    public interface ChatNavigationClickListener{

        void onChatNavigationItemClicked(int selectedPosition,ChatListItem selectedItem);

        void closeDrawer();

    }
}
