package com.vedamic.usmobiletheapp_alpha.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;

import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = "==> SearchActivity";

    private Constants.DashboardPageState searchContext = null;

    private Toolbar toolbar;
    private EditText searchTextField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        this.searchTextField = (EditText) this.toolbar.findViewById(R.id.toolbarSearchTextField);
        this.searchTextField.requestFocus();

        //get the search context
        try{
            this.searchContext = (Constants.DashboardPageState) getIntent().getSerializableExtra(Constants.DASHBOARD_PAGE_STATE);

            //set the edittext based on the search context
            switch (this.searchContext){
                case CHAT:
                    this.searchTextField.setHint("Search chats.. ");
                    break;
                case CONTACT:
                    this.searchTextField.setHint("Search contacts.. ");
                    break;
                case CALL:
                    this.searchTextField.setHint("Search calls.. ");
                    break;
                case CONFERENCE:
                    this.searchTextField.setHint("Search conference.. ");
                    break;
                default:
                    throw new SearchContextMisMatchException("Search Context "+this.searchContext+" Not recognized.");
            }


        }catch (Exception e){
            finish();
            Log.e(TAG, "onCreate: --> "+e.getMessage());
        }


    }

    private void findViews(){

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);

    }

    public class SearchContextMisMatchException extends Exception{

        public SearchContextMisMatchException(String detailMessage) {
            super(detailMessage);
        }
    }
}
