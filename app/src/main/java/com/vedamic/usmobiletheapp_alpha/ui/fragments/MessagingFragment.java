package com.vedamic.usmobiletheapp_alpha.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.app.objects.ChatListItem;
import com.vedamic.usmobiletheapp_alpha.app.objects.DummyItem;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hamm.pinnedsectionlistview.PinnedSectionListView;

/**
 * Created by yogesh on 4/21/16.
 */
public class MessagingFragment extends Fragment {
    /*
     ------------------------------------------------------------------------------
     | Activity must provide the Contact or the chat id that is to be loaded by
     | this messaging fragment
     ------------------------------------------------------------------------------
    */
    private ChatListItem selectedChatSession;

    //the views
    private PinnedSectionListView messageListView;
    private ImageView messagingBackground;
    private ImageButton moreButton,smileyButton,sendButton;
    private EditText messageContent;

    private PinnedListViewAdapter adapter;
    private List<DummyItem> itemList = new ArrayList<>();

    private Calendar calendar;

    public MessagingFragment() {
    }

    public static MessagingFragment newInstance(ChatListItem selectedChatSession){
        MessagingFragment messagingFragment = new MessagingFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.SELECTED_CHAT_SESSION, selectedChatSession);
        messagingFragment.setArguments(args);
        return messagingFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            this.selectedChatSession = (ChatListItem) getArguments().getSerializable(Constants.SELECTED_CHAT_SESSION);
        }

        calendar = Calendar.getInstance();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_messaging,container,false);
        findViews(view);

        return view;

    }

    private void findViews(View view){
        this.messagingBackground = (ImageView) view.findViewById(R.id.messageBackground);
        //set the messaging background
        Picasso.with(getActivity())
                .load(Constants.ASSET_IMAGE_DIR+"message_background.jpg")
                .into(this.messagingBackground);

        this.messageContent = (EditText) view.findViewById(R.id.messageContent);

        this.sendButton = (ImageButton) view.findViewById(R.id.messageSend);
        this.sendButton.setEnabled(false);

        this.messageListView = (PinnedSectionListView) view.findViewById(R.id.messageListview);
        this.messageListView.setShadowVisible(false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.messageContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (messageContent.getText().toString().length() > 0) {
                    sendButton.setEnabled(true);
                } else {
                    sendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        generateDataset();
        this.adapter = new PinnedListViewAdapter(getActivity());
        this.messageListView.setAdapter(this.adapter);
        moveToLastItemInList();


        this.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //add the message to the messageList
                DummyItem item = new DummyItem(DummyItem.ITEM, messageContent.getText().toString(),calendar.getTime().toString(),DummyItem.SENT);
                itemList.add(item);
//                adapter.add(item);
                moveToLastItemInList();

                //then invalidate the edit text
                messageContent.setText("");
                sendButton.setEnabled(false);
            }
        });
    }

    private void moveToLastItemInList(){
        this.messageListView.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                messageListView.setSelection(messageListView.getCount() - 1);
            }
        });
    }


    public void generateDataset(){

        final int sectionsNumber = 4;

        String[] dates = new String[]{"2015-12-30","2016-01-03","Yesterday","Today"};

        for (int i=0; i < sectionsNumber; i++) {

            DummyItem section = new DummyItem(DummyItem.SECTION, dates[i]);
            this.itemList.add(section);

            for (int j=0;j<(i + 2);j++) {
                DummyItem item = new DummyItem(DummyItem.ITEM,
                        getActivity().getResources().getString(R.string.lorem_sh),"11:30 pm",(j%2 == 0)?DummyItem.SENT:DummyItem.RECV);
                this.itemList.add(item);
            }
        }
    }


    private class PinnedListViewAdapter extends ArrayAdapter<DummyItem> implements PinnedSectionListView.PinnedSectionListAdapter{

        public PinnedListViewAdapter(Context context) {
            super(context, R.layout.dummy_chat_bubble_sent, itemList);
        }

        @Override
        public boolean isItemViewTypePinned(int viewType) {
            return viewType == DummyItem.SECTION;
        }

        @Override
        public DummyItem getItem(int position) {
            return itemList.get(position);
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            return getItem(position).type;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            DummyItem item = getItem(position);

            if (item.type == DummyItem.SECTION) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.dummy_section_header,parent,false);
                TextView header = (TextView) convertView.findViewById(R.id.pinText);
                header.setText(item.text);
            }
            else{
                if(item.MSG_BUBBLE_TYPE == DummyItem.SENT){
                    convertView = getActivity().getLayoutInflater().inflate(R.layout.dummy_chat_bubble_sent,parent,false);
                }
                else{
                    convertView = getActivity().getLayoutInflater().inflate(R.layout.dummy_chat_bubble_received,parent,false);
                    ImageView author = (ImageView) convertView.findViewById(R.id.messageAuthorPic);
                    Picasso.with(getActivity())
                            .load(selectedChatSession.getContactDetail().getProfileThumbImageUri())
                            .resize(50,50)
                            .centerCrop()
                            .into(author);
                }

                TextView message = (TextView) convertView.findViewById(R.id.messageContent);

                message.setText(item.text);
            }

            return convertView;
        }
    }


}
