package com.vedamic.usmobiletheapp_alpha.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telecom.Call;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.app.objects.CallListItem;
import com.vedamic.usmobiletheapp_alpha.tasks.QueryCallSession;
import com.vedamic.usmobiletheapp_alpha.ui.activity.StartCallSession;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class CallListFragment extends Fragment {

    private static final String TAG = "==> CallListFragment";

    //the layouts
    private LinearLayout emptylayout,successlayout;
    private ListView callListView;

    private List<CallListItem> callListItems = new ArrayList<>();
    private QueryCallSession queryCallSession;

    public CallListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_call_list, container, false);
        findViews(view);

        return view;
    }

    private void findViews(View view){
        this.emptylayout = (LinearLayout) view.findViewById(R.id.noCallSessionLayout);
        this.successlayout = (LinearLayout) view.findViewById(R.id.callListLayout);
        this.callListView = (ListView) view.findViewById(R.id.callListView);

        setViewState(Constants.ViewState.LOADING);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.queryCallSession = getQueryCallSession();
        this.queryCallSession.execute();

        this.callListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //call list item clicked, carry appropriate action
                Intent intent = new Intent(getActivity(), StartCallSession.class);
                intent.putExtra(Constants.SELECTED_CONTACT,callListItems.get(i).getContactDetail());
                startActivity(intent);
            }
        });

    }

    private QueryCallSession getQueryCallSession(){
        return new QueryCallSession(getActivity()) {
            @Override
            public void onPostExecuteCall(Boolean aBoolean, List<CallListItem> callListItemList) {
                callListItems = callListItemList;
                if(aBoolean){
                    //successfully queried results
                    callListView.setAdapter(new CallListAdapter());
                    setViewState(Constants.ViewState.SUCCESS);
                }
                else{
                    //failed to query results
                    setViewState(Constants.ViewState.EMPTY);
                }
            }
        };
    }

    private class CallListAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return callListItems.size();
        }

        @Override
        public Object getItem(int i) {
            return callListItems.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if(view == null){
                view = getActivity().getLayoutInflater().inflate(R.layout.item_call_list_view_img,viewGroup,false);
            }

            CallListItem item = (CallListItem) getItem(i);
            ImageView profile = (ImageView) view.findViewById(R.id.callItemProfilePic);
            ImageView icon = (ImageView) view.findViewById(R.id.callItemIcon);
            TextView name = (TextView) view.findViewById(R.id.callItemName);
            TextView date = (TextView) view.findViewById(R.id.callItemDate);
            TextView type = (TextView) view.findViewById(R.id.callItemType);
            TextView duration = (TextView) view.findViewById(R.id.callItemDuration);

            Picasso.with(getActivity())
                    .load(item.getContactDetail().getProfileThumbImageUri())
                    .placeholder(R.drawable.ic_icon_account_placeholder)
                    .into(profile);

            switch (item.getCallType()){
                case INCOMMING:
                    icon.setImageResource(R.drawable.ic_icon_call_in_sm);
                    type.setText("Incoming");
                    break;
                case OUTGOING:
                    icon.setImageResource(R.drawable.ic_icon_call_out_sm);
                    type.setText("Outgoing");
                    break;
                case MISSED:
                    icon.setImageResource(R.drawable.ic_icon_call_missed_sm);
                    type.setText("Missed");
                    break;
                default:
                    icon.setVisibility(View.GONE);
                    type.setVisibility(View.GONE);
                    break;
            }

            name.setText(item.getContactDetail().getName());
            date.setText(item.getDate());
            duration.setText(item.getDuration());

            return view;
        }

        @Override
        public int getItemViewType(int position) {
            return super.getItemViewType(position);
        }

        @Override
        public int getViewTypeCount() {
            return callListItems.size();
        }
    }


    private void setViewState(Constants.ViewState viewState){
        switch (viewState){
            case SUCCESS:
                this.emptylayout.setVisibility(View.GONE);
                this.successlayout.setVisibility(View.VISIBLE);
                break;
            case FAILED:
            case EMPTY:
                this.emptylayout.setVisibility(View.VISIBLE);
                this.successlayout.setVisibility(View.GONE);
                break;
            default:
            case LOADING:
                this.emptylayout.setVisibility(View.GONE);
                this.successlayout.setVisibility(View.GONE);
                break;
        }
    }



}
