package com.vedamic.usmobiletheapp_alpha.utils;

/**
 * Created by yogesh on 4/19/16.
 */
public class Constants {

    public static final String DASHBOARD_PAGE_STATE = "dashboard_page_state";

    public enum DashboardPageState{
        CHAT,CONTACT,CALL,CONFERENCE
    }

    public enum ViewState{
        LOADING,EMPTY,FAILED,SUCCESS
    }

    public static final String ASSET_IMAGE_DIR = "file:///android_asset/images/";

    public static final String SELECTED_CHAT_SESSION = "selected_chat_session";

    public static final String SELECTED_CONTACT      = "selected contact";

    public static final String SELECTED_CALL_TYPE    = "selected call type";
    public enum CallType{
        OUTGOING,INCOMMING,MISSED
    }

    public enum CallingStates{
        CALLING,RINGING,TALKING,ENDING
    }
}
