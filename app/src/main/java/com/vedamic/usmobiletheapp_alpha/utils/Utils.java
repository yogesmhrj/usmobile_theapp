package com.vedamic.usmobiletheapp_alpha.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.PowerManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by yogesh on 4/22/16.
 */
public class Utils {

    /**
     * Get the height of the status bar if available
     *
     * @param activity
     * @return
     */
    public static int getStatusBarHeightRes(Activity activity) {
        int result = 0;
        //build version lower than Lollipop wont have status bar available
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT){
            int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = activity.getResources().getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }

    /**
     *
     * Get the current date with provided date format pattern.
     * <br>
     *     Default pattern is yyyy-MMM-dd.
     * <br><br>
     *     You can use patterns such as :
     *     <ul>
     *         <li>MMMM dd, yyyy
     *         <li>dd/MM/yyyy
     *         <li>yyyy/MM/dd HH:mm:ss</li>
     *         <li>EEE, MMM d, yyyy</li>
     *         <li>and other combinations</li>
     *     </ul>
     *
     * @param format
     *      The format of the date
     * @return
     *      Date with the requested date format
     */
    public static String getCurrentDate(String format){
        Calendar c = Calendar.getInstance();
        if(format == null){
            format = "yyyy-MMM-dd";
        }
        SimpleDateFormat df = new SimpleDateFormat(format, Locale.ENGLISH);
        return df.format(c.getTime());
    }


    public static void sleep(long timeMilli){
        try {
            Thread.sleep(timeMilli);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void keepScreenOn(Activity activity) {
        PowerManager pm = (PowerManager) activity.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        wl.acquire();

        // screen and CPU will stay awake during this section until release has been invoked
        wl.release();
    }
}
