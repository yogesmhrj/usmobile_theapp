package com.vedamic.usmobiletheapp_alpha.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by yogesh on 4/19/16.
 */
public class Helpers {
    private static final String TAG = "==> Helpers";

    public static boolean makeCall(Activity context,String phoneNumber){
        try {

            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + phoneNumber));
            context.startActivity(intent);
            return true;
        }catch (SecurityException e){
            Log.e(TAG, "makeCall: "+e.getMessage());
            alertNeutral(context,"Permission Denied!","Permission to make call with the app has not been granted. Please make sure the permission for the app to make calls has been granted before you continue.");
            return false;
        }
    }

    public static void alertNeutral(Activity activity,String title,String message){
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setCancelable(true)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    /**
     * Method to set the height of the list view based on the total number of the children.
     * <br><br>
     * This is useful when the ListView is being implemented inside a
     * <b>ScrollView or NestedScrollView   </b>
     * <br><br>
     * The method will make use of the adapter set to the listview thus make sure that this method
     * is called after the <b>setAdapter()</b> for the listview has been called before implementing this method.
     *
     * @param listView
     *      The list view whose height is to be set.
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

}
