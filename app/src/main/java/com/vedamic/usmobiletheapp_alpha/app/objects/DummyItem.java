package com.vedamic.usmobiletheapp_alpha.app.objects;

/**
 * Created by yogesh on 4/22/16.
 */
public class DummyItem {

    public static final int ITEM = 0;
    public static final int SECTION = 1;

    public static final int SENT = 123;
    public static final int RECV = 345;

    public final int type;
    public final String text;
    public String date = "";
    public int MSG_BUBBLE_TYPE = SENT;

    public int sectionPosition;
    public int listPosition;

    public DummyItem(int type, String text) {
        this.type = type;
        this.text = text;
    }

    public DummyItem(int type, String text, String date,int msgBubbleType) {
        this.type = type;
        this.text = text;
        this.date = date;
        this.MSG_BUBBLE_TYPE = msgBubbleType;
    }

    @Override public String toString() {
        return text;
    }
}
