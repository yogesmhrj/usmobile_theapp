package com.vedamic.usmobiletheapp_alpha.app.core;

import android.app.Application;

import com.vedamic.usmobiletheapp_alpha.R;
import com.vedamic.usmobiletheapp_alpha.app.objects.ChatListItem;
import com.vedamic.usmobiletheapp_alpha.app.objects.ContactDetail;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by yogesh on 4/21/16.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //required to set the custom font in the application
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/roboto_light.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }

    /*
         ------------------------------------------------------------------------------
         | dummy contact list
         ------------------------------------------------------------------------------
        */
    public static List<ContactDetail> getDummyContactLIst(){
        List<ContactDetail> items = new ArrayList<>();

        items.add(new ContactDetail(0,"Robert Pike","9841123456","2015-05-01", Constants.ASSET_IMAGE_DIR+"profile_pic1.jpg","something@something.com"));
        items.add(new ContactDetail(1,"Kyle Adams","9841123456","2015-05-01", Constants.ASSET_IMAGE_DIR+"profile_pic2.jpg","something@something.com"));
        items.add(new ContactDetail(2,"Rich Smith","9841123456","2015-05-01", Constants.ASSET_IMAGE_DIR+"profile_pic3.jpeg","something@something.com"));
        items.add(new ContactDetail(3,"Rob Luke","9841123456","2015-05-01", Constants.ASSET_IMAGE_DIR+"profile_pic4.jpeg","something@something.com"));
        items.add(new ContactDetail(4,"Chris Alex","9841123456","2015-05-01", Constants.ASSET_IMAGE_DIR+"profile_pic5.jpg","something@something.com"));
        items.add(new ContactDetail(5,"John Snow","9841123456","2015-05-01", Constants.ASSET_IMAGE_DIR+"profile_pic6.jpg","something@something.com"));

        return items;
    }

    /*
     ------------------------------------------------------------------------------
     | Dummy data for chat list
     ------------------------------------------------------------------------------
    */
    public static List<ChatListItem> getDummyChatList(){
        List<ChatListItem> items = new ArrayList<>();
        List<ContactDetail> contacts = getDummyContactLIst();

        for (int i = 0; i < 6; i++) {
            items.add(new ChatListItem(i,contacts.get(i),
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor",
                    String.valueOf((i +1) * 5),0,false));

        }


        return items;
    }

}
