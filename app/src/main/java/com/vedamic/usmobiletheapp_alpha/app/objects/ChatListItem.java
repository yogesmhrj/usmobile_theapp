package com.vedamic.usmobiletheapp_alpha.app.objects;

import java.io.Serializable;

/**
 * Created by yogesh on 4/21/16.
 */
public class ChatListItem implements Serializable{

    private long chatId;
    private ContactDetail contactDetail;
    private String message;
    private String dateTime;
    private int notificationCount = 0;
    private boolean NEW_MSG_ACTIVE = false;

    public ChatListItem(long chatId, ContactDetail contactDetail, String message, String dateTime, int notificationCount, boolean NEW_MSG_ACTIVE) {
        this.chatId = chatId;
        this.contactDetail = contactDetail;
        this.message = message;
        this.dateTime = dateTime;
        this.notificationCount = notificationCount;
        this.NEW_MSG_ACTIVE = NEW_MSG_ACTIVE;
    }

    public ChatListItem(long chatId, ContactDetail contactDetail, String message, String dateTime) {
        this.chatId = chatId;
        this.contactDetail = contactDetail;
        this.message = message;
        this.dateTime = dateTime;
    }

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }

    public ContactDetail getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(ContactDetail contactDetail) {
        this.contactDetail = contactDetail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }

    public boolean isNEW_MSG_ACTIVE() {
        return NEW_MSG_ACTIVE;
    }

    public void setNEW_MSG_ACTIVE(boolean NEW_MSG_ACTIVE) {
        this.NEW_MSG_ACTIVE = NEW_MSG_ACTIVE;
    }

    @Override
    public String toString() {
        return "ChatListItem{" +
                "chatId='" + chatId + '\'' +
                ", contactDetail=" + contactDetail +
                ", message='" + message + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", notificationCount=" + notificationCount +
                ", NEW_MSG_ACTIVE=" + NEW_MSG_ACTIVE +
                '}';
    }
}
