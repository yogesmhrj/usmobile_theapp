package com.vedamic.usmobiletheapp_alpha.app.objects;

import java.io.Serializable;

/**
 * Created by yogesh on 4/21/16.
 */
public class ContactDetail implements Serializable {

    private long contactId;
    private String name;
    private String phone;
    private String joinedDate;
    private String profileThumbImageUri;
    private String profileImageUri;
    private String email;

    public ContactDetail(long contactId, String name, String phone, String profileThumbImageUri) {
        this.contactId = contactId;
        this.name = name;
        this.phone = phone;
        this.profileThumbImageUri = profileThumbImageUri;
    }

    public ContactDetail(long contactId, String name, String phone, String joinedDate, String profileThumbImageUri, String email) {
        this.contactId = contactId;
        this.name = name;
        this.phone = phone;
        this.joinedDate = joinedDate;
        this.profileThumbImageUri = profileThumbImageUri;
        this.profileImageUri = profileThumbImageUri;
        this.email = email;
    }

    public ContactDetail(long contactId, String name, String phone, String joinedDate, String profileThumbImageUri, String profileImageUri, String email) {
        this.contactId = contactId;
        this.name = name;
        this.phone = phone;
        this.joinedDate = joinedDate;
        this.profileThumbImageUri = profileThumbImageUri;
        this.profileImageUri = profileImageUri;
        this.email = email;
    }

    public long getContactId() {
        return contactId;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getJoinedDate() {
        return joinedDate;
    }

    public void setJoinedDate(String joinedDate) {
        this.joinedDate = joinedDate;
    }

    public String getProfileThumbImageUri() {
        return profileThumbImageUri;
    }

    public void setProfileThumbImageUri(String profileThumbImageUri) {
        this.profileThumbImageUri = profileThumbImageUri;
    }

    public String getProfileImageUri() {
        return profileImageUri;
    }

    public void setProfileImageUri(String profileImageUri) {
        this.profileImageUri = profileImageUri;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
