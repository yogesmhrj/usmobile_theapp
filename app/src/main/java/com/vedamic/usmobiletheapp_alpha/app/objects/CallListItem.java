package com.vedamic.usmobiletheapp_alpha.app.objects;

import com.vedamic.usmobiletheapp_alpha.utils.Constants;

import java.io.Serializable;

/**
 * Created by yogesh on 4/26/16.
 */
public class CallListItem implements Serializable{

    private long id;
    private ContactDetail contactDetail;
    private Constants.CallType callType;
    private String date;
    private String duration;

    public CallListItem(ContactDetail contactDetail, Constants.CallType callType, String date, String duration) {
        this.contactDetail = contactDetail;
        this.callType = callType;
        this.date = date;
        this.duration = duration;
    }

    public CallListItem(long id, ContactDetail contactDetail, Constants.CallType callType, String date, String duration) {
        this.id = id;
        this.contactDetail = contactDetail;
        this.callType = callType;
        this.date = date;
        this.duration = duration;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ContactDetail getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(ContactDetail contactDetail) {
        this.contactDetail = contactDetail;
    }

    public Constants.CallType getCallType() {
        return callType;
    }

    public void setCallType(Constants.CallType callType) {
        this.callType = callType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "CallListItem{" +
                "id=" + id +
                ", contactDetail=" + contactDetail +
                ", callType=" + callType +
                ", date='" + date + '\'' +
                ", duration='" + duration + '\'' +
                '}';
    }
}
