package com.vedamic.usmobiletheapp_alpha.app.exceptions;

/**
 * Created by yogesh on 4/26/16.
 *
 * Call this Exception when the parameters required by any activity is missing during initialization
 *
 */
public class ActivityMemberIncompleteException extends Exception{

    private static String TAG = "ActivityMemberIncompleteException : Some parameters are required by the activity to work properly, which have not been provided.";

    public ActivityMemberIncompleteException() {
        super(TAG);
    }

    public ActivityMemberIncompleteException(String detailMessage) {
        super(TAG+"\n --> "+detailMessage);
    }
}
