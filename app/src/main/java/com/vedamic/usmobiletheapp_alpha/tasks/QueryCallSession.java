package com.vedamic.usmobiletheapp_alpha.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.vedamic.usmobiletheapp_alpha.app.core.BaseApplication;
import com.vedamic.usmobiletheapp_alpha.app.objects.CallListItem;
import com.vedamic.usmobiletheapp_alpha.app.objects.ContactDetail;
import com.vedamic.usmobiletheapp_alpha.utils.Constants;
import com.vedamic.usmobiletheapp_alpha.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yogesh on 4/26/16.
 */
public abstract class QueryCallSession extends AsyncTask<ContactDetail,Void,Boolean> {

    private List<CallListItem> callListItems = new ArrayList<>();
    private boolean enableProgressDialog = true;
    private ProgressDialog dialog;
    private Activity activity;

    public QueryCallSession(Activity activity) {
        this.activity = activity;
    }

    public void setEnableProgressDialog(boolean enableProgressDialog) {
        this.enableProgressDialog = enableProgressDialog;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(this.enableProgressDialog){
            this.dialog = new ProgressDialog(this.activity);
            this.dialog.setMessage("Loading call history...");
            this.dialog.setCancelable(false);
            this.dialog.setCanceledOnTouchOutside(false);
            this.dialog.show();
        }
    }

    @Override
    protected Boolean doInBackground(ContactDetail... contactDetails) {
        //query for the call session
        if(contactDetails == null || contactDetails.length == 0){

        }
        else{

        }

        for (ContactDetail contactDetail : BaseApplication.getDummyContactLIst()) {
            this.callListItems.add(new CallListItem(contactDetail, Constants.CallType.INCOMMING, Utils.getCurrentDate(null),"12:34"));
            this.callListItems.add(new CallListItem(contactDetail, Constants.CallType.OUTGOING, Utils.getCurrentDate("MMMM dd, yyyy"),"0:55"));
            this.callListItems.add(new CallListItem(contactDetail, Constants.CallType.MISSED, Utils.getCurrentDate("yyyy/MM/dd HH:mm:ss"),"00:00"));
        }

        return !this.callListItems.isEmpty();
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if(this.dialog.isShowing()){
            this.dialog.dismiss();
        }
        onPostExecuteCall(aBoolean,this.callListItems);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        onPostExecute(false);
    }

    public abstract void onPostExecuteCall(Boolean aBoolean, List<CallListItem> callListItemList);

    public List<CallListItem> getCallListItems() {
        return callListItems;
    }
}
